import os.path

import cherrypy


@cherrypy.expose
class Main(object):

    @cherrypy.expose
    def upload(self, filepath, folder_name, sub_folder_name):
        file = open(filepath, mode='rb')
        read = file.read()
        filename = file.name.split('/')[-1]
        new_path = folder_name + sub_folder_name
        f = open(new_path+filename, "wb")
        f.write(read)
        f.close()
        return 'success'

    @cherrypy.expose
    def getfile(self, file_name, folder_name, sub_folder_name):
        file = folder_name + sub_folder_name + file_name
        file = open(file, mode='rb')
        read = file.read()
        cherrypy.response.headers['Content-Disposition'] = 'attachment; filename=' + file_name + ''
        return read


cherrypy.quickstart(Main())


# /home/local/ZOHOCORP/mounika-pt4319/
# Desktop/uploads/
